for i in {1..10}
do
	echo creating ds $i x 100
	for j in {1..100}
	do
		cat daemonset.yaml | sed 's/replaceme/'$(($j + $i * 100 ))'/g' >> 100bagger-${i}.yaml
	done
	kubectl apply -f 100bagger-${i}.yaml
done
sleep 120
for i in {1..10}
do
        echo deleting $i
        kubectl delete --wait -f 100bagger-${i}.yaml
done
rm -f 100bagger*.yaml
