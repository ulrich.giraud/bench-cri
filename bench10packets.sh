for i in {1..10}
do
	echo creating ds $i x 10
	for j in {1..10}
	do
		cat daemonset.yaml | sed 's/replaceme/'$(($j + $i * 10 ))'/g' >> 10bagger-${i}.yaml
	done
	kubectl apply -f 10bagger-${i}.yaml
done
sleep 120
for i in {1..10}
do
        echo deleting $i
        kubectl delete --wait -f 10bagger-${i}.yaml
done
rm -f 10bagger*.yaml
